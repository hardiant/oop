<?php
require('animal.php');
require('frog.php');
require('ape.php');

echo "RELEASE 0" . "<br>";
echo "---------------------" . "<br>";
$sheep = new Animal("Shaun");
echo "Nama Hewan = ".$sheep->name ."<br>";
echo "Jumlah Kaki = ".$sheep->legs ."<br>";
echo "Berdarah Dingin = ".$sheep->cold_blooded ."<br> <br>";

echo "RELEASE 1" . "<br>";
echo "---------------------" . "<br>";

$sheep = new Animal("Shaun");
echo "Nama Hewan = ".$sheep->name ."<br>";
echo "Jumlah Kaki = ".$sheep->legs ."<br>";
echo "Berdarah Dingin = ".$sheep->cold_blooded ."<br> <br>";

$kodok = new Frog("Buduk");
echo "Nama Hewan = ".$kodok->name ."<br>";
echo "Jumlah Kaki = ".$kodok->legs ."<br>";
echo "Berdarah Dingin = ".$kodok->cold_blooded ."<br>";
echo  $kodok->jump() . "<br><br>";


$kera = new Ape("Kera Sakti");
echo "Nama Hewan = ".$kera->name ."<br>";
echo "Jumlah Kaki = ".$kera->legs ."<br>";
echo "Berdarah Dingin= ".$kera->cold_blooded ."<br>";
echo  $kera->yell() . "<br><br>";


?>
